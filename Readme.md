##### Flask Hello World local host docker image



After reading this tutorial:

https://gitlab.bio.di.uminho.pt/tutorials/python-flask-docker-ci-helloworld-example





1. Created folder Structure.
2. Created `app2.py`

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello1():
    return 'Hello1 from /'

@app.route('/api')
def hello2():
    return '{"id":0,"message":"Flask: Hello World from Docker"}'

if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0')
```

3. Created `Dockerfile`

```dockerfile
FROM python:latest

ENV APPPATH /opt/myflaskapp
COPY . $APPPATH
WORKDIR $APPPATH/app

RUN buildDeps='python-pip python-dev build-essential' \
    && set -x \
    && apt-get update \
	&& apt-get install -y $buildDeps \
 	&& pip install --upgrade pip \
 	&& pip install -r requirements.txt \
 	&& apt-get clean \
 	&& rm -rf /var/lib/apt/lists/* \
 	&& apt-get purge -y --auto-remove $buildDeps

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["src/app2.py"]
```



4. Created  `requirements.txt` file:

```
Flask
```

5. Created `.gitlab-ci.yml` file:

```bash
stages:
    - build

docker-build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    -  echo $CI_BUILD_TOKEN | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
```



6. Created git repository

```bash
git init
git status
git add .
git commit -m "First commit"
git push --set-upstream git@gitlab.com:Andrejkf/flask2.git master
```

6.1 Change "private" repository to "public".

Settings -> General -> Visibility, project features, permissions -> Project Visibility 

And save changes.

7. If pipeline went right then :

    Packages and registries -> Container Registry

    Then copy that container registry:

    `registry.gitlab.com/andrejkf/flask2`

8. Then docker pull from GitLab registry:

```bash
docker pull registry.gitlab.com/andrejkf/flask2
```

9. Then check docker images and containers:

```bash
docker images
docker ps -a
docker ps
```

10. Then create container:

```bash
docker run -d -p 5000:5000 registry.gitlab.com/andrejkf/flask2
```

or you can use:

```bash
docker run -it -p 5000:5000 registry.gitlab.com/andrejkf/flask2
```



11. Then go to `localhost:5000` and test it out.

